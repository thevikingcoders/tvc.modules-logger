import moment from 'moment';
import _ from 'lodash';

const predefined_types = ['debug', 'info', 'warning', 'error', 'critical', 'fatal'];

class Logger {

	constructor(options = {}, output) {
		this.options = {
			"console": true,
			"logSeparator": " - ",
			"timestampFormat": "YYYY-MM-DD HH:mm:ss.SSS",
			"paddingLength": 8,
			"paddingCharacter": " ",
			"stringifySpaces": 0
		};

		_.forEach(options, (value, key) => {
			if (_.keys(this.options).indexOf(key) > -1) this.options[key] = value;
		});

		this.output = output ? output : null;
		predefined_types.map(type => this.register(type));

		this.on_events = {};
	}

	register(type) {
		this[type] = (...args) => this.log(type, args);
	}

	log(type, args) {

		let str = this.assembleLine(type, args);

		if (this.options.console) {

			if (this.output) {

				if (typeof this.output == 'object' && this.output.indexOf(type) > -1) {
					console.log(str);
				}
				else if (this.output == type) {
					console.log(str);
				}

			} else {
				console.log(str);
			}

			this.checkOn(type, str);

		} else {

			this.checkOn(type, str);

		}
	}

	pre(str) {
		this.prelog = str;
		return this;
	}

	ip(ip) {
		this.ipAddress = ip;
		return this;
	}

	assembleLine(type, args) {
		let ts = moment().format(this.options.timestampFormat);
		let line = [`[${ts}]`, `[${this.pad(type).toUpperCase()}]`];

		if (this.prelog) {
			line.push(`[${this.prelog}]`);
			this.prelog = null;
		}

		if (this.ipAddress) {
			line.push(`[${this.ipAddress}]`);
			this.ipAddress = null;
		}

		args = args.map(arg => this.stringify(arg));
		line.push(args.join(this.options.logSeparator));

		return line.join(' ');
	}

	pad(str) {
		while (str.length < this.options.paddingLength)
			str += this.options.paddingCharacter;
		return str;
	}

	on(type, cb, return_line = false) {
		this.on_events[type] = {cb: cb, return_line: return_line};
	}

	removeOn(type) {
		delete this.on_events[type];
	}

	checkOn(type, line) {
		if (this.on_events[type]) {
			if (this.on_events[type].return_line) {
				this.on_events[type].cb(line);
			} else {
				this.on_events[type].cb();
			}
		}
	}

	stringify(obj) {
		return typeof obj === 'object' ? JSON.stringify(obj, null, this.options.stringifySpaces) : obj;
	}

	get ipAddress() {
		return this._ipAddress;
	}

	set ipAddress(value) {
		this._ipAddress = value;
	}

	get prelog() {
		return this._prelog;
	}

	set prelog(value) {
		this._prelog = value;
	}

	get output() {
		return this._output;
	}

	set output(value) {
		this._output = value;
	}

}
export default Logger