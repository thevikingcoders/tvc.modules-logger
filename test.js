import Logger from './tvc.logger';

const log = new Logger({timestampFormat: "HH:mm:ss.SSS"}, ['raw', 'debug', 'url']);
const log2 = new Logger({timestampFormat: "HH:mm:ss", paddingLength: 6});

log.register('raw');

function extraLog() {
	['debug', 'info', 'warning', 'error', 'critical', 'fatal'].map(type => log[type](`${type} test`));
}

function rawLog(str) {
	log.raw(str);
}

log.register('url');
log.on('url', extraLog);
log.on('info', rawLog, true);

log.ip('8.8.8.8').pre('pre-test').url({snarf: 'blast'}, '8.8.8.8');

log.removeOn('info');

log.info('removeOn test');

log2.register('server');

log2.info('Bootstrap complete');
